package br.com.mikesantos.MsSkyHomes.Listeners;

import org.bukkit.Bukkit;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;

public class PlayerListeners implements Listener{

	private MsSkyHomes plugin;

	public PlayerListeners(MsSkyHomes plugin, boolean registerEvents) {
		super();
		this.plugin = plugin;
		if(registerEvents){
			Bukkit.getPluginManager().registerEvents(this, plugin);
		}
	}
	
	@EventHandler
	public void Join(PlayerJoinEvent e){
		this.plugin.getDatabaseManager().loadUser(e.getPlayer());
	}
	
	
}

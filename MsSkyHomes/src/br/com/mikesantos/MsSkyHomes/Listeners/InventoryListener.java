package br.com.mikesantos.MsSkyHomes.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;
import br.com.mikesantos.MsSkyHomes.Shop;
import br.com.mikesantos.MsSkyHomes.User;
import br.com.mikesantos.MsSkyHomes.Managers.VoteType;
import br.com.mikesantos.MsSkyHomes.Utils.DelayUtilities;
import br.com.mikesantos.MsSkyHomes.Utils.Messages;
import br.com.mikesantos.MsSkyHomes.Utils.PrivateDelayUtilities;
import br.com.mikesantos.MsSkyHomes.Utils.Packets.NBTItemStack;

@SuppressWarnings("deprecation")
public class InventoryListener implements Listener{

	protected MsSkyHomes plugin;
	protected PrivateDelayUtilities delay;
	public InventoryListener(MsSkyHomes plugin, boolean registerEvents) {
		super();
		this.plugin = plugin;
		if(registerEvents)
			Bukkit.getPluginManager().registerEvents(this, plugin);
		this.delay = new PrivateDelayUtilities();
	}
	
	@EventHandler
	public void InventoryClose(InventoryCloseEvent e){
		if(!(e.getPlayer() instanceof Player)) return;
		String title = e.getInventory().getTitle();
		if(title.equalsIgnoreCase(Messages.getInstance().getInventoryName()) || title.equalsIgnoreCase(Messages.getInstance().getInventoryOptionsName()) || title.startsWith("Loja de")){
			((Player)e.getPlayer()).playSound(e.getPlayer().getLocation(), Sound.CHEST_CLOSE, 1, 1);
		}
	}
	
	@EventHandler
	public void InventoryClick(InventoryClickEvent e){
		if(!(e.getWhoClicked() instanceof Player)) return;
		String title = e.getInventory().getTitle();
		if(title.equalsIgnoreCase(Messages.getInstance().getInventoryName()) || title.equalsIgnoreCase(Messages.getInstance().getInventoryOptionsName()) || title.startsWith("Loja de")){
			((Player)e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.CLICK, 1, 1);
		}
	}
	
	@EventHandler
	public void InventoryCheck(InventoryClickEvent e){
		if(!(e.getWhoClicked() instanceof Player)) return;
		String title = e.getInventory().getTitle();
		if(title.equalsIgnoreCase(Messages.getInstance().getInventoryName()) || title.equalsIgnoreCase(Messages.getInstance().getInventoryOptionsName())){
			if(e.getRawSlot() >= e.getInventory().getSize()) return;
			if(e.getSlot() == -999) return;
			final Player p = (Player)e.getWhoClicked();
			e.setCancelled(true);
			new BukkitRunnable() {
				
				@Override
				public void run() {
					p.updateInventory();
				}
			}.runTaskLater(this.plugin, 20L);
			ItemStack item = e.getCurrentItem();
			if(item != null && item.getType() != Material.AIR){
				NBTItemStack packet = new NBTItemStack(item);
				if(packet.hasKey("MsHomeName")){
					String home = packet.getString("MsHomeName");
					User user = this.plugin.getDatabaseManager().getUserByPlayer(p);
					if(packet.hasKey("MsHomeMain")){
						if(e.isLeftClick()){
							Location loc = user.getHome(home);
							if(loc == null){
								p.sendMessage(Messages.getInstance().getMessageTeleportError());
							}else{
								p.teleport(loc);
								p.sendMessage(Messages.getInstance().getMessageTeleportSuccess(home));
							}
							p.closeInventory();
						}else if(e.isRightClick()){
							this.plugin.getInventoryManager().openHomeOptions(p, home);
						}
					}else if(packet.hasKey("MsHomeTeleport")){
						Location loc = user.getHome(home);
						if(loc == null){
							p.sendMessage(Messages.getInstance().getMessageTeleportError());
						}else{
							p.teleport(loc);
							p.sendMessage(Messages.getInstance().getMessageTeleportSuccess(home));
						}
						p.closeInventory();
					}else if(packet.hasKey("MsHomeDelete")){
						user.removeHome(home);
						p.sendMessage(Messages.getInstance().getMessageHomeDeleted(home));
						p.closeInventory();
					}else if(packet.hasKey("MsHomeChangePublic")){
						if(!p.hasPermission("msadvancedhomes.public.change")){
							p.sendMessage(Messages.getInstance().getMessageNotHasPermissionToChangePublic());
							return;
						}
						if(DelayUtilities.noHasDelay(p)){
							boolean mode = packet.getBoolean("MsHomesSetPublic");
							if(mode){
								user.setPublic(home);
							}else{
								user.removePublic(home);
							}
							e.getInventory().setItem(e.getSlot(), this.plugin.getInventoryManager().getItemPublic(mode, home));
							DelayUtilities.setDelay(p, 10);
						}else{
							p.sendMessage(Messages.getInstance().getMessageChangePublicWaitDelay(DelayUtilities.getDelay(p)));
						}
					}
				}else if(packet.hasKey("MsHomeCloseMain")){
					p.closeInventory();
				}else if(packet.hasKey("MsHomeReturnToMain")){
					this.plugin.getInventoryManager().openAllHomes(p);
				}
			}
		}else if(title.startsWith("Loja de")){
			if(e.getRawSlot() >= e.getInventory().getSize()) return;
			if(e.getSlot() == -999) return;
			final Player p = (Player)e.getWhoClicked();
			e.setCancelled(true);
			new BukkitRunnable() {
				
				@Override
				public void run() {
					p.updateInventory();
				}
			}.runTaskLater(this.plugin, 20L);
			ItemStack item = e.getCurrentItem();
			if(item != null && item.getType() != Material.AIR){
				NBTItemStack packet = new NBTItemStack(item);
				if(packet.hasKey("MsShopName") && packet.hasKey("MsShopOwnerName")){
					String shopName = packet.getString("MsShopName");
					String owner = packet.getString("MsShopOwnerName");
					if(e.isLeftClick()){
						User user = this.plugin.getDatabaseManager().getUserByUsername(owner);
						if(user.isPublic(shopName)){
							Location loc = user.getPublicHome(shopName);
							if(loc == null){
								p.sendMessage(Messages.getInstance().getMessageShopTeleportError(shopName));
							}else{
								p.teleport(loc);
								p.sendMessage(Messages.getInstance().getMessageShopTeleportSuccess(shopName, owner));
							}
							p.closeInventory();
						}else{
							p.sendMessage(Messages.getInstance().getMessageShopNotFound(shopName));
						}
					}else{
						this.plugin.getInventoryManager().openShopVotes(p, owner, shopName);
					}
				}else if(packet.hasKey("MsHomeCloseMain")){
					p.closeInventory();
				}
			}
		}else if(title.startsWith("�bVotos #")){
			if(e.getRawSlot() >= e.getInventory().getSize()) return;
			if(e.getSlot() == -999) return;
			final Player p = (Player)e.getWhoClicked();
			e.setCancelled(true);
			new BukkitRunnable() {
				
				@Override
				public void run() {
					p.updateInventory();
				}
			}.runTaskLater(this.plugin, 20L);
			ItemStack item = e.getCurrentItem();
			if(item != null && item.getType() != Material.AIR){
				NBTItemStack packet = new NBTItemStack(item);
				if(packet.hasKey("VoteType") && packet.hasKey("ShopName") && packet.hasKey("ShopOwner")){
					VoteType type = VoteType.getByName(packet.getString("VoteType"));
					String owner = packet.getString("ShopOwner");
					String shopName = packet.getString("ShopName");
					Shop shop = this.plugin.getVotesManager().getShopsByUserName(owner).getShop(shopName);
					if(this.delay.noHasDelay(p)){
						this.delay.setDelay(p, 5);
					}else{
						p.sendMessage("�cTente novamente em " + this.delay.getDelay(p));
						return;
					}
					if(!this.plugin.getVotesManager().canVote(p, shop)){
						p.sendMessage("�cVoc� j� avaliou esta loja, ent�o n�o pode pode avaliar novamente!");
						return;
					}
					this.plugin.getVotesManager().vote(type, shop, p);
					p.sendMessage("�eVoc� deu " + (type == VoteType.BAD ? "�cDislike" : "�aLike") + " �enessa loja!");
					p.closeInventory();
				}
			}
		}
	}
	
}

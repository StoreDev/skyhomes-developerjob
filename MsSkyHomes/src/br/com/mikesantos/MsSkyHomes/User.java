package br.com.mikesantos.MsSkyHomes;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import br.com.mikesantos.MsSkyHomes.API.Events.PlayerDeleteHomeEvent;
import br.com.mikesantos.MsSkyHomes.API.Events.PlayerDeletePublicHomeEvent;
import br.com.mikesantos.MsSkyHomes.API.Events.PlayerSetHomeEvent;
import br.com.mikesantos.MsSkyHomes.API.Events.PlayerSetPublicHomeEvent;
import br.com.mikesantos.MsSkyHomes.Utils.LocationUtilities;

public class User {

	private String playerName;
	private HashMap<String, Location> homes = new HashMap<String, Location>();
	private List<String> publicHomes = new ArrayList<String>();
	
	public User(String user){
		this.playerName = user;
	}
	
	public String getPlayerName(){
		return this.playerName;
	}
	
	public Player getPlayer(){
		return Bukkit.getPlayer(getPlayerName());
	}
	
	public void addHome(String name, Location location, boolean isPublic){
		homes.put(name, location);
		if(isPublic){
			publicHomes.add(name);
		}
	}
	
	public void setHome(final String name, final Location location){
		new Thread(()->{
			try {
				PreparedStatement sql = MsSkyHomes.getPlugin().database.getConnection().prepareStatement("INSERT INTO mshomes(owner, name, location, public) VALUES (?,?,?,?);");
				sql.setString(1, getPlayerName());
				sql.setString(2, name);
				sql.setString(3, LocationUtilities.serializeLocation(location));
				sql.setBoolean(4, false);
				sql.execute();
				homes.put(name, location);
				callSetHomeEvent(location, name);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		/*
		new Thread(new BukkitRunnable() {
			
			@Override
			public void run() {
				try {
					PreparedStatement sql = MsSkyHomes.getPlugin().database.getConnection().prepareStatement("INSERT INTO mshomes(owner, name, location, public) VALUES (?,?,?,?);");
					sql.setString(1, getPlayerName());
					sql.setString(2, name);
					sql.setString(3, LocationUtilities.serializeLocation(location));
					sql.setBoolean(4, false);
					sql.execute();
					homes.put(name, location);
					callSetHomeEvent(location, name);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();*/
	}
	
	public boolean hasHome(String name){
		for(String s : homes.keySet()){
			if(s.equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}
	
	public void removeHome(final String name){
		new Thread(new BukkitRunnable() {
			
			@Override
			public void run() {
				if(hasHome(name)){
					try {
						PreparedStatement sql = MsSkyHomes.getPlugin().database.getConnection().prepareStatement("DELETE FROM mshomes WHERE owner=? AND name=?;");
						sql.setString(1, getPlayerName());
						sql.setString(2, name);
						sql.execute();
						
						homes.remove(name);
						if(publicHomes.contains(name)){
							publicHomes.remove(name);
						}
						
						MsSkyHomes.getPlugin().getVotesManager().deleteVotes(name, getPlayerName());
						callDeleteHomeEvent(null, name);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public void setPublic(final String name){
		new Thread(new BukkitRunnable() {
			
			@Override
			public void run() {
				if(hasHome(name)){
					try {
						PreparedStatement sql = MsSkyHomes.getPlugin().database.getConnection().prepareStatement("UPDATE mshomes SET public=? WHERE owner=? AND name=?;");
						sql.setBoolean(1, true);
						sql.setString(2, getPlayerName());
						sql.setString(3, name);
						sql.execute();
						publicHomes.add(name);
						callSetPublicHomeEvent(null, name);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public void removePublic(final String name){
		new Thread(new BukkitRunnable() {
			
			@Override
			public void run() {
				if(hasHome(name)){
					try {
						PreparedStatement sql = MsSkyHomes.getPlugin().database.getConnection().prepareStatement("UPDATE mshomes SET public=? WHERE owner=? AND name=?;");
						sql.setBoolean(1, false);
						sql.setString(2, getPlayerName());
						sql.setString(3, name);
						sql.execute();
						publicHomes.remove(name);
						callDeletePublicHomeEvent(null, name);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public boolean isPublic(String name){
		for(String s : publicHomes){
			if(s.equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}
	
	public Location getHome(String name){
		if(hasHome(name)){
			return homes.get(name);
		}
		return null;
	}
	
	public Location getPublicHome(String name){
		if(hasHome(name)){
			if(isPublic(name)){
				return homes.get(name);
			}
		}
		return null;
	}
	
	public HashMap<String, Location> getAllHomes(){
		return homes;
	}
	
	public boolean canSetHome(int size){
		if(getAllHomes().size() >= size){
			return false;
		}
		return true;
	}
	
	
	
	
	
	protected void callSetHomeEvent(final Location loc, final String home){
		new BukkitRunnable() {
			
			@Override
			public void run() {
				PlayerSetHomeEvent event = new PlayerSetHomeEvent(getPlayer(), home, loc, User.this);
				Bukkit.getPluginManager().callEvent(event);
			}
		}.runTask(MsSkyHomes.getPlugin());
	}
	
	protected void callDeleteHomeEvent(final Location location, final String home){
		new BukkitRunnable() {
			
			@Override
			public void run() {
				PlayerDeleteHomeEvent event = new PlayerDeleteHomeEvent(getPlayer(), home, location, User.this);
				Bukkit.getPluginManager().callEvent(event);
			}
		}.runTask(MsSkyHomes.getPlugin());
	}
	
	protected void callSetPublicHomeEvent(final Location location, final String home){
		new BukkitRunnable() {
			
			@Override
			public void run() {
				PlayerSetPublicHomeEvent event = new PlayerSetPublicHomeEvent(getPlayer(), home, location, User.this);
				Bukkit.getPluginManager().callEvent(event);
			}
		}.runTask(MsSkyHomes.getPlugin());
	}
	
	protected void callDeletePublicHomeEvent(final Location location, final String home){
		new BukkitRunnable() {
			
			@Override
			public void run() {
				PlayerDeletePublicHomeEvent event = new PlayerDeletePublicHomeEvent(getPlayer(), home, location, User.this);
				Bukkit.getPluginManager().callEvent(event);
			}
		}.runTask(MsSkyHomes.getPlugin());
	}
}

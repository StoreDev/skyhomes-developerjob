package br.com.mikesantos.MsSkyHomes.Managers;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;
import br.com.mikesantos.MsSkyHomes.Shop;
import br.com.mikesantos.MsSkyHomes.Shops;
import br.com.mikesantos.MsSkyHomes.Utils.PrivateDelayUtilities;

public class VotesManager {

	protected MsSkyHomes plugin;
	protected PrivateDelayUtilities delay;
	public VotesManager(MsSkyHomes plugin) {
		super();
		this.plugin = plugin;
		this.delay = new PrivateDelayUtilities();
	}
	
	public void vote(final VoteType type, final Shop shop, final Player p){
		if(this.delay.noHasDelay(p)){
			this.delay.setDelay(p, 5);
		}else{
			p.sendMessage("�cTente novamente em " + this.delay.getDelay(p));
			return;
		}
		new Thread(new BukkitRunnable() {
			
			@Override
			public void run() {
				try {
					PreparedStatement sql = plugin.database.getConnection().prepareStatement("INSERT INTO mshomes_votes_history(shop, owner, voter_name, voter_ip) VALUES(?,?,?,?);");
					sql.setString(1, shop.getName());
					sql.setString(2, shop.getOwnerName());
					sql.setString(3, p.getName());
					sql.setString(4, p.getAddress().getHostName());
					sql.execute();
					
					PreparedStatement add = plugin.database.getConnection().prepareStatement("INSERT INTO mshomes_votes(shop, owner, vote_status) VALUES(?,?,?);");
					add.setString(1, shop.getName());
					add.setString(2, shop.getOwnerName());
					add.setString(3, type.getName());
					add.execute();
					
					shop.addVote(type);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public void deleteVotes(final String shop, final String owner){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					PreparedStatement sql = plugin.database.getConnection().prepareStatement("DELETE FROM mshomes_votes WHERE shop=? AND owner=?;");
					sql.setString(1, shop);
					sql.setString(2, owner);
					sql.execute();
					
					PreparedStatement sql2 = plugin.database.getConnection().prepareStatement("DELETE FROM mshomes_votes_history WHERE shop=? AND owner=?;");
					sql2.setString(1, shop);
					sql2.setString(2, owner);
					sql2.execute();
					
					getShopsByUserName(owner).getShop(shop).clearVotes();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public boolean canVote(Player p, Shop shop){
		try {
			PreparedStatement sql = this.plugin.database.getConnection().prepareStatement("SELECT * FROM mshomes_votes_history WHERE shop=? AND owner=?");
			sql.setString(1, shop.getName());
			sql.setString(2, shop.getOwnerName());
			ResultSet rs = sql.executeQuery();
			 
			String player_name = p.getName();
			String player_ip = p.getAddress().getHostName();
			while(rs.next()){
				String voter_ip = rs.getString("voter_ip");
				String voter_name = rs.getString("voter_name");
				if(voter_ip.equals(player_ip) || voter_name.equalsIgnoreCase(player_name)){
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	protected ConcurrentHashMap<String, Shops> cache = new ConcurrentHashMap<String, Shops>();
	private Shops load(String p){
		String p_name = p;
		Shops shops = new Shops(p_name);
		try {
			PreparedStatement sql = this.plugin.database.getConnection().prepareStatement("SELECT * FROM mshomes_votes WHERE owner=?;");
			sql.setString(1, p_name);
			ResultSet rs = sql.executeQuery();
			while(rs.next()){
				String shopName = rs.getString("shop");
				VoteType voteType = VoteType.getByName(rs.getString("vote_status"));
				if(shops.hasShop(shopName)){
					shops.getShop(shopName).addVote(voteType);
				}else{
					Shop shop = new Shop(p_name, shopName);
					shop.addVote(voteType);
					shops.addShop(shop);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();}
		return shops;
	}
	
	public Shops getShopsOfPlayer(Player p){
		return getShopsByUserName(p.getName());
	}
	
	public Shops getShopsByUserName(String username){
		Shops shop = cacheGet(username);
		if(shop == null){
			shop = load(username);
			cache.put(username, shop);
		}
		return shop;
	}
	
	private Shops cacheGet(String p){
		for(Shops shop : cache.values()){
			if(shop.getOwner().equalsIgnoreCase(p)){
				return shop;
			}
		}
		return null;
	}
}

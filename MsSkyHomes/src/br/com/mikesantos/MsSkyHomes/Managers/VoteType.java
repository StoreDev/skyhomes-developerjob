package br.com.mikesantos.MsSkyHomes.Managers;

public enum VoteType {
	GOOD("good"), BAD("bad");
	
	protected String name;
	private VoteType(String name) {
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public static VoteType getByName(String name){
		for(VoteType type : values()){
			if(type.getName().equalsIgnoreCase(name)){
				return type;
			}
		}
		return null;
	}
}

package br.com.mikesantos.MsSkyHomes.Managers;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;
import br.com.mikesantos.MsSkyHomes.Shop;
import br.com.mikesantos.MsSkyHomes.User;
import br.com.mikesantos.MsSkyHomes.Utils.Messages;
import br.com.mikesantos.MsSkyHomes.Utils.Utils;
import br.com.mikesantos.MsSkyHomes.Utils.Packets.NBTItemStack;

public class InventoryManager {

	private MsSkyHomes plugin;

	public InventoryManager(MsSkyHomes plugin) {
		super();
		this.plugin = plugin;
	}
	
	protected int[] glasses = {1,2,3,4,5,6,7}; 
	
	public void openAllHomes(Player p){
		User user = this.plugin.getDatabaseManager().getUserByPlayer(p);
		Inventory inv = Bukkit.createInventory(null, 54, Messages.getInstance().getInventoryName());
		inv.setItem(0, getHead(p.getName()));
		inv.setItem(8, getCloseButton());
		
		ItemStack glass = new ItemStack(Material.THIN_GLASS, 1 , (short)8);
		ItemMeta meta = glass.getItemMeta();
		meta.setDisplayName("�0");
		glass.setItemMeta(meta);
		for(int i : glasses){
			inv.setItem(i, glass);
		}
		
		user.getAllHomes().keySet().forEach( (s) ->{
			ItemStack item = getItem(s);
			if(inv.firstEmpty() != -1){
				inv.setItem(inv.firstEmpty(), item);
			}
		});
		
		while(inv.firstEmpty() != -1){
			inv.setItem(inv.firstEmpty(), glass);
		}
		p.openInventory(inv);
		p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
	}
	
	public void openHomeOptions(Player p, String home){
		User user = this.plugin.getDatabaseManager().getUserByPlayer(p);
		Inventory inv = Bukkit.createInventory(null, 36, Messages.getInstance().getInventoryOptionsName());
		
		ItemStack teleporte = new ItemStack(Material.ENDER_PEARL);
		ItemMeta meta_teleporte = teleporte.getItemMeta();
		meta_teleporte.setDisplayName("�aTeleporte");
		meta_teleporte.setLore(Arrays.asList("", "�3Clique para teleportar at� a home", "�b" + home));
		teleporte.setItemMeta(meta_teleporte);
		
		ItemStack deletar = new ItemStack(Material.WOOL, 1, (byte)14);
		ItemMeta meta_deletar = deletar.getItemMeta();
		meta_deletar.setDisplayName("�cDeletar");
		meta_deletar.setLore(Arrays.asList("","�3Clique para deletar a home", "�b" + home));
		deletar.setItemMeta(meta_deletar);
		
		ItemStack publico = new ItemStack(Material.ENDER_CHEST);
		ItemMeta meta_public = publico.getItemMeta();
		meta_public.setDisplayName("�eP�blico");
		meta_public.setLore(Arrays.asList("", "�aDeixar os players irem at� a home", "�b" + home));
		publico.setItemMeta(meta_public);
		
		NBTItemStack packet_teleporte = new NBTItemStack(teleporte);
		NBTItemStack packet_deletar = new NBTItemStack(deletar);
		
		packet_teleporte.setString("MsHomeName", home);
		packet_teleporte.setBoolean("MsHomeTeleport", true);
		
		packet_deletar.setString("MsHomeName", home);
		packet_deletar.setBoolean("MsHomeDelete", true);
		
		inv.setItem(11, packet_teleporte.getItem());
		inv.setItem(13, publico);
		inv.setItem(15, packet_deletar.getItem());
		inv.setItem(22, getItemPublic(user.isPublic(home), home));
		inv.setItem(inv.getSize() - 1, getPreviewMain());
		
		ItemStack glass = new ItemStack(Material.THIN_GLASS, 1 , (short)8);
		ItemMeta meta = glass.getItemMeta();
		meta.setDisplayName("�0");
		glass.setItemMeta(meta);
		while(inv.firstEmpty() != -1){
			inv.setItem(inv.firstEmpty(), glass);
		}
		
		p.openInventory(inv);
		p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1, 1);
	}
	
	public void openShopOfPlayerToPlayer(User owner, Player see){
		Inventory inv = Bukkit.createInventory(null, 54, Messages.getInstance().getShopInventoryName(owner.getPlayerName()));
		int current = 9;
		inv.setItem(0, getShopHead(owner.getPlayerName()));
		for(String s : owner.getAllHomes().keySet()){
			if(owner.isPublic(s)){
				ItemStack item = getShopItem(s, owner.getPlayerName());
				inv.setItem(current, item);
				current++;
			}
		}
		inv.setItem(8, getCloseButton());
		
		ItemStack glass = new ItemStack(Material.THIN_GLASS, 1 , (short)8);
		ItemMeta meta = glass.getItemMeta();
		meta.setDisplayName("�0");
		glass.setItemMeta(meta);
		while(inv.firstEmpty() != -1){
			inv.setItem(inv.firstEmpty(), glass);
		}
		
		see.openInventory(inv);
		see.playSound(see.getLocation(), Sound.CHEST_OPEN, 1, 1);
	}
	
	public void openShopVotes(Player see, String owner, String shop){
		Inventory inv = Bukkit.createInventory(null, 9, "�bVotos #" + (shop.length() > 6 ? shop.substring(0, 6) : shop));
		
		ItemStack vote_positive = new ItemStack(Material.WOOL, 1, (byte)5); 
		ItemStack vote_negative = new ItemStack(Material.WOOL, 1, (byte)14);
		ItemStack vote_total = new ItemStack(Material.BOOK);
		
		ItemMeta vote_positive_meta = vote_positive.getItemMeta();
		ItemMeta vote_negative_meta = vote_negative.getItemMeta();
		ItemMeta vote_total_meta = vote_total.getItemMeta();
		
		vote_positive_meta.setDisplayName("�a�lGostei");
		vote_positive_meta.setLore(Arrays.asList("","�bClique aqui para avaliar esta loja!"));
		vote_negative_meta.setDisplayName("�c�lN�o Gostei");
		vote_negative_meta.setLore(Arrays.asList("","�bClique aqui para avaliar esta loja!"));
		
		Shop obj = this.plugin.getVotesManager().getShopsByUserName(owner).getShop(shop);
		vote_total_meta.setDisplayName("�0");
		vote_total_meta.setLore(Arrays.asList("�aGostei: " + String.valueOf(obj.getVotes(VoteType.GOOD)), "�cN�o Gostei: " + String.valueOf(obj.getVotes(VoteType.BAD))));
		
		vote_positive.setItemMeta(vote_positive_meta);
		vote_negative.setItemMeta(vote_negative_meta);
		vote_total.setItemMeta(vote_total_meta);
		
		NBTItemStack packet_positive = new NBTItemStack(vote_positive);
		NBTItemStack packet_negative = new NBTItemStack(vote_negative);
		
		packet_positive.setString("VoteType", VoteType.GOOD.getName());
		packet_positive.setString("ShopName", shop);
		packet_positive.setString("ShopOwner", owner);
		
		packet_negative.setString("VoteType", VoteType.BAD.getName());
		packet_negative.setString("ShopName", shop);
		packet_negative.setString("ShopOwner", owner);
		
		inv.setItem(0, packet_positive.getItem());
		inv.setItem(8, packet_negative.getItem());
		inv.setItem(4, vote_total);
		
		ItemStack glass = new ItemStack(Material.THIN_GLASS, 1 , (short)8);
		ItemMeta meta = glass.getItemMeta();
		meta.setDisplayName("�0");
		glass.setItemMeta(meta);
		while(inv.firstEmpty() != -1){
			inv.setItem(inv.firstEmpty(), glass);
		}
		
		see.openInventory(inv);
		see.playSound(see.getLocation(), Sound.CHEST_OPEN, 1, 1);
	}
	
	public ItemStack getItemPublic(boolean isPublic, String home){
		ItemStack item = new ItemStack(Material.INK_SACK, 1, (isPublic ? (byte)10 : (byte)8));
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Messages.getInstance().getItemOptionsPublicName(isPublic));
		item.setItemMeta(meta);
		NBTItemStack packet = new NBTItemStack(item);
		packet.setBoolean("MsHomesSetPublic", (isPublic ? false : true));
		packet.setString("MsHomeName", home);
		packet.setBoolean("MsHomeChangePublic", true);
		return packet.getItem();
	}
	
	private ItemStack getCloseButton(){
		ItemStack item = new ItemStack(Material.WOOL, 1, (byte)14);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Messages.getInstance().getCloseButtonName());
		item.setItemMeta(meta);
		NBTItemStack packet = new NBTItemStack(item);
		packet.setBoolean("MsHomeCloseMain", true);
		return packet.getItem();
	}
	
	private ItemStack getPreviewMain(){
		ItemStack item = new ItemStack(Material.WOOL, 1, (byte)5);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Messages.getInstance().getPreviusMain());
		item.setItemMeta(meta);
		NBTItemStack packet = new NBTItemStack(item);
		packet.setBoolean("MsHomeReturnToMain", true);
		return packet.getItem();
	}
	
	private ItemStack getHead(String pName){
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte)3);
		SkullMeta meta = (SkullMeta)item.getItemMeta();
		meta.setOwner(pName);
		meta.setDisplayName(Messages.getInstance().getHeadName(pName));
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack getShopHead(String pName){
		ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (byte)3);
		SkullMeta meta = (SkullMeta)item.getItemMeta();
		meta.setOwner(pName);
		meta.setDisplayName(Messages.getInstance().getShopHeadName(pName));
		item.setItemMeta(meta);
		return item;
	}
	
	private ItemStack getItem(String homeName){
		ItemStack item = Messages.getInstance().getItemHome();
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Messages.getInstance().getItemHomeName(homeName));
		meta.setLore(Utils.replace("(?i)<shopname>", homeName, Messages.getInstance().getItemHomeLore(homeName)));
		item.setItemMeta(meta);
		NBTItemStack packet = new NBTItemStack(item);
		packet.setString("MsHomeName", homeName);
		packet.setBoolean("MsHomeMain", true);
		return packet.getItem();
	}
	
	private ItemStack getShopItem(String shopName, String ownerName){
		ItemStack item = Messages.getInstance().getItemShop();
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Messages.getInstance().getItemShopName(shopName));
		meta.setLore(Utils.replace("(?i)<shopName>", shopName, Messages.getInstance().getItemShopLore(shopName)));
		item.setItemMeta(meta);
		
		NBTItemStack packet = new NBTItemStack(item);
		packet.setString("MsShopName", shopName);
		packet.setString("MsShopOwnerName", ownerName);
		return packet.getItem();
	}
}

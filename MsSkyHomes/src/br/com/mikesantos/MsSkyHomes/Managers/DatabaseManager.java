package br.com.mikesantos.MsSkyHomes.Managers;

import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;
import br.com.mikesantos.MsSkyHomes.User;
import br.com.mikesantos.MsSkyHomes.Utils.LocationUtilities;

public class DatabaseManager {

	private MsSkyHomes plugin;

	public DatabaseManager(MsSkyHomes plugin) {
		super();
		this.plugin = plugin;
		startup();
	}
	
	protected HashMap<String, User> cache = new HashMap<String, User>();
	public void startup(){
		try {
			PreparedStatement sql = this.plugin.database.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `mshomes` (`owner` text, `name` text, `location` text, `public` BOOLEAN DEFAULT FALSE);");
			PreparedStatement sql2 = this.plugin.database.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `mshomes_votes` (`shop` text, `owner` text, `vote_status` text);");
			PreparedStatement sql3 = this.plugin.database.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `mshomes_votes_history` (`shop` text, `owner` text, `voter_name` text, `voter_ip` text);");
			sql.execute();
			sql2.execute();
			sql3.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public User loadUser(Player p){
		return loadUser(p.getName());
	}
	
	public User loadUser(String playerName){
		User user = new User(playerName);
		try {
			PreparedStatement sql = this.plugin.database.getConnection().prepareStatement("SELECT * FROM mshomes WHERE owner=?;");
			sql.setString(1, user.getPlayerName());
			ResultSet rs = sql.executeQuery();
			while(rs.next()){
				String name = rs.getString("name");
				boolean isPublic = rs.getBoolean("public");
				Location location = LocationUtilities.deserializeLocation(rs.getString("location"));
				user.addHome(name, location, isPublic);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		cache.put(playerName, user);
		return user;
	}
	
	public User getUserByPlayer(Player p){
		User user = cache.get(p.getName());
		if(user == null){
			user = loadUser(p);
			cache.put(p.getName(), user);
		}
		return user;
	}
	
	public User getUserByUsername(String username){
		for(String s : cache.keySet()){
			if(s.equalsIgnoreCase(username)){
				return cache.get(s);
			}
		}
		return null;
	}
	
}

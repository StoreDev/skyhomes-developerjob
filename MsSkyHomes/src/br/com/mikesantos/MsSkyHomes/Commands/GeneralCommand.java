package br.com.mikesantos.MsSkyHomes.Commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;
import br.com.mikesantos.MsSkyHomes.User;
import br.com.mikesantos.MsSkyHomes.Utils.Messages;

public class GeneralCommand implements CommandExecutor{

	public MsSkyHomes plugin;
	public GeneralCommand(MsSkyHomes plugin, boolean registerCommands) {
		super();
		this.plugin = plugin;
		if(registerCommands){
			plugin.getCommand("home").setExecutor(this);
			plugin.getCommand("sethome").setExecutor(this);
			//plugin.getCommand("lojas").setExecutor(this);
			plugin.getCommand("loja").setExecutor(this);
			plugin.getCommand("mshome").setExecutor(this);
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if(!(sender instanceof Player)){
			sender.sendMessage("�cSomente Players!");
			return true;
		}
		if(cmd.getName().equalsIgnoreCase("mshome")){
			if(sender instanceof Player){
				if(!sender.hasPermission("msskyhomes.administrator")){
					sender.sendMessage(Messages.getInstance().getMessageNoPermissionCommandAdministrator());
					return true;
				}
			}
			if(args.length == 0){
				sender.sendMessage("�e[�6MsSkyHomes�e] �6Comandos:");
				sender.sendMessage("  �e/mshomes reload �f- �6Recarrega todas as configura��es");
				return true;
			}
			if(args[0].equalsIgnoreCase("reload")){
				this.plugin.reloadConfig();
				Messages.getInstance().clearMessages();
				sender.sendMessage("�e[�6MsSkyHomes�e] �6Plugin Recarregado com sucesso!");
			}
			sender.sendMessage("�e[�6MsSkyHomes�e] �cComando nao encontrado!");
			return true;
		}else
		if(cmd.getName().equalsIgnoreCase("home")){
			Player p = (Player)sender;
			User user = this.plugin.getDatabaseManager().getUserByPlayer(p);
			if(args.length == 0){
				this.plugin.getInventoryManager().openAllHomes(p);
				return true;
			}
			String home = args[0];
			if(!user.hasHome(home)){
				sender.sendMessage(Messages.getInstance().getMessageHomeNotFound(home));
				return true;
			}
			Location loc = user.getHome(home);
			if(loc == null){
				p.sendMessage(Messages.getInstance().getMessageTeleportError());
			}else{
				p.teleport(loc);
				p.sendMessage(Messages.getInstance().getMessageTeleportSuccess(home));
			}
			return true;
		}else if(cmd.getName().equalsIgnoreCase("sethome")){
			Player p = (Player)sender;
			User user = this.plugin.getDatabaseManager().getUserByPlayer(p);
			if(args.length != 1){
				sender.sendMessage(Messages.getInstance().getMessageSetHomeHelp());
				return true;
			}
			boolean canContinue = true;
			if(!user.canSetHome(3)){
				canContinue = false;
				if(!p.hasPermission("msskyhomes.homes.limit.bypass")){
					for(int i = 1; i != 50; i++){
						if(p.hasPermission("msskyhomes.homes.limit." + i)){
							canContinue = true;
							break;
						}
					}
				}else{
					canContinue = true;
				}
			}
			if(!canContinue){
				sender.sendMessage(Messages.getInstance().getMessageCantSetMoreHomes_LimitReached(user.getAllHomes().size()));
				return true;
			}
			String argHome = args[0];
			if(user.hasHome(argHome)){
				sender.sendMessage(Messages.getInstance().getMessageHomeSetError(argHome));
				return true;
			}
			user.setHome(argHome, p.getLocation());
			sender.sendMessage(Messages.getInstance().getMessageHomeSetted(argHome));
			return true;
		}else if(cmd.getName().equalsIgnoreCase("loja")){
			Player p = (Player)sender;
			if(args.length != 1){
				sender.sendMessage(Messages.getInstance().getMessageShopHelp());
				return true;
			}
			String arg = args[0];
			User user2 = this.plugin.getDatabaseManager().getUserByUsername(arg);
			if(user2 == null){
				user2 = this.plugin.getDatabaseManager().loadUser(arg);
			}
			if(user2 == null){
				sender.sendMessage(Messages.getInstance().getMessageErrorShopNotFound(arg));
				return true;
			}
			if(user2.getAllHomes().size() <= 0){
				sender.sendMessage(Messages.getInstance().getMessageErrorShopNotFound(arg));
				return true;
			}
			this.plugin.getInventoryManager().openShopOfPlayerToPlayer(user2, p);
			return true;
		}
		return false;
	}
	
}

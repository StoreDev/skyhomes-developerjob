package br.com.mikesantos.MsSkyHomes;

import java.io.File;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import br.com.mikesantos.MsSkyHomes.API.API;
import br.com.mikesantos.MsSkyHomes.Commands.GeneralCommand;
import br.com.mikesantos.MsSkyHomes.Database.Database;
import br.com.mikesantos.MsSkyHomes.Listeners.InventoryListener;
import br.com.mikesantos.MsSkyHomes.Listeners.PlayerListeners;
import br.com.mikesantos.MsSkyHomes.Managers.DatabaseManager;
import br.com.mikesantos.MsSkyHomes.Managers.InventoryManager;
import br.com.mikesantos.MsSkyHomes.Managers.VotesManager;

public class MsSkyHomes extends JavaPlugin{

	public Database database;
	public void onEnable() {
		saveResource("permissoes.txt", true);
		saveDefaultConfig();
		reloadConfig();
		if(getConfig().getBoolean("MySQL.Ativar", false)){
			String host = getConfig().getString("MySQL.Host", "localhost");
			String user = getConfig().getString("MySQL.User", "user");
			String pass = getConfig().getString("MySQL.Password", "pass");
			String db = getConfig().getString("MySQL.Database", "msadvancedhomes");
			database = new Database(host, db, user, pass);
			if(!database.isOpen()){
				connectSQLite();
			}
		}else{
			connectSQLite();
		}
		//database = new Database("localhost", "msskyhomes", "root", "vertrigo");
		dbmanager = new DatabaseManager(this);
		invmanager = new InventoryManager(this);
		votesmanager = new VotesManager(this);
		new GeneralCommand(this, true);
		new PlayerListeners(this, true);
		new InventoryListener(this, true);
		//new SignGUI(this);
		this.api = new API(this);
		Bukkit.getConsoleSender().sendMessage("�bMsSkyHomes Enabled with Success!");
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		database.close();
		super.onDisable();
	}
	
	protected FileConfiguration config;
	@Override
	public FileConfiguration getConfig(){
		return this.config;
	}
	
	@Override
	public void reloadConfig() {
		this.config = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml"));
	}
	
	@Override
	public void saveConfig() {
		try {
			this.config.save(new File(getDataFolder(), "config.yml"));
			reloadConfig();
		} catch (Exception e) {}
	}
	
	protected DatabaseManager dbmanager;
	public DatabaseManager getDatabaseManager(){
		return this.dbmanager;
	}
	
	protected InventoryManager invmanager;
	public InventoryManager getInventoryManager(){
		return this.invmanager;
	}
	
	protected VotesManager votesmanager;
	public VotesManager getVotesManager(){
		return this.votesmanager;
	}
	
	protected API api;
	public API getAPI(){
		return this.api;
	}
	
	
	public static MsSkyHomes getPlugin(){
		return (MsSkyHomes)Bukkit.getPluginManager().getPlugin("MsSkyHomes");
	}
	
	public void connectSQLite(){
		File defaultFolder = getDataFolder();
		if(!defaultFolder.exists()){
			defaultFolder.mkdirs();
		}
		File SQLite = new File(getDataFolder(), "SQLite.db");
		if(!SQLite.exists()){
			try {
				SQLite.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		database = new Database(SQLite);
	}
}

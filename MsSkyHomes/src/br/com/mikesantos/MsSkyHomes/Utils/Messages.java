package br.com.mikesantos.MsSkyHomes.Utils;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;

public class Messages {

	protected static Messages instance = null;
	
	public static Messages getInstance(){
		if(instance == null){
			instance = new Messages();
		}
		return instance;
	}
	
	public String getInventoryName(){
		return "Suas Homes";
	}
	
	public String getInventoryOptionsName(){
		return "Op��es Home";
	}
	
	public String getHeadName(String pName){
		return String.format(getMessage("Home Item.Head Name"), pName);
	}
	
	public String getItemHomeName(String home){
		return String.format(getMessage("Home Item.Name"), home);
	}
	
	public List<String> getItemHomeLore(String name){
		return getStringList("Home Item.Description");
	}
	
	public ItemStack getItemHome(){
		String s = getMessage("Home Item.ID");
		int id = 0;
		byte data = 0;
		String[] split = s.split(":");
		id = Integer.parseInt(split[0]);
		if(split.length == 2){
			data = Byte.parseByte(split[1]);
		}
		return new ItemStack(Material.getMaterial(id), 1 , (byte)data);
	}
	
	public ItemStack getItemShop(){
		String s = getMessage("Shop Item.ID");
		int id = 0;
		byte data = 0;
		String[] split = s.split(":");
		id = Integer.parseInt(split[0]);
		if(split.length == 2){
			data = Byte.parseByte(split[1]);
		}
		return new ItemStack(Material.getMaterial(id), 1 , (byte)data);
	}
	
	public String getShopInventoryName(String pName){
		String name = "Loja de " + pName;
		return (name.length() > 16 ? name.substring(0,  16) : name);
	}
	
	public String getShopHeadName(String pName){
		return String.format(getMessage("Shop Item.Head Name"), pName);
	}
	
	public String getItemShopName(String shop){
		return String.format(getMessage("Shop Item.Name"), shop);
	}
	
	public List<String> getItemShopLore(String shop){
		return getStringList("Shop Item.Description");
	}
	
	public String getMessageShopTeleportSuccess(String shopName, String owner){
		return String.format(getMessage("Teleporting For Shop"), shopName, owner);
	}
	
	public String getMessageShopTeleportError(String shopName){
		return String.format(getMessage("Shop Not Found"), shopName);
	}
	
	public String getMessageShopNotFound(String shopName){
		return String.format(getMessage("Home Isnt Public"), shopName);
	}
	
	public String getCloseButtonName(){
		return getMessage("Button Close Name");
	}
	
	public String getPreviusMain(){
		return getMessage("Button Previous Name");
	}
	
	public String getItemOptionsPublicName(boolean isPublic){
		return (isPublic ? getMessage("Change Properties.Change Private") : getMessage("Change Properties.Change Public"));
	}
	
	public String getMessageChangePublicWaitDelay(String duration){
		return String.format(getMessage("Wait For Change Home Properties Again"), duration);
	}
	
	public String getMessageTeleportSuccess(String home){
		return String.format(getMessage("Teleported for Home"), home);
	}
	
	public String getMessageTeleportError(){
		return getMessage("Error While Teleport for Home");
	}
	
	public String getMessageHomeDeleted(String home){
		return String.format(getMessage("Home Successfully Removed"), home);
	}
	
	public String getMessageHomeSetted(String home){
		return String.format(getMessage("Home Successfully Set"), home);
	}
	
	public String getMessageHomeSetError(String home){
		return String.format(getMessage("Home Already Exists"), home);
	}
	
	public String getMessageSetHomeHelp(){
		return getMessage("Command SetHome Usage");
	}
	
	public String getMessageHomeNotFound(String home){
		return String.format(getMessage("Home Not Found"), home);
	}
	
	public String getMessageShopHelp(){
		return getMessage("Command Shop Usage");
	}
	
	public String getMessageErrorShopNotFound(String owner){
		return String.format(getMessage("Player Shop Not Found"), owner);
	}
	
	public String getMessageCantSetMoreHomes_LimitReached(int total){
		return String.format(getMessage("Limit Reached"), total);
	}
	
	public String getMessageNotHasPermissionToChangePublic(){
		return getMessage("No Permission.Change Public");
	}
	
	public String getMessageNoPermissionCommandAdministrator(){
		return getMessage("No Permission.Administrator Command");
	}
	
	protected HashMap<String, String> cache = new LinkedHashMap<String, String>();
	protected HashMap<String, List<String>> listcache = new LinkedHashMap<String, List<String>>();
	private String getMessage(String path){
		if(cache.containsKey(path)){
			return cache.get(path);
		}
		String message = MsSkyHomes.getPlugin().getConfig().getString("Messages." + path, "&cMensagem n�o encontrada!");
		String prefix = MsSkyHomes.getPlugin().getConfig().getString("Messages.Prefix");
		prefix = ChatColor.translateAlternateColorCodes('&', prefix);
		message = ChatColor.translateAlternateColorCodes('&', message.replaceAll("(?i)<prefix>", prefix));
		cache.put(path, message);
		return getMessage(path);
	}
	
	private List<String> getStringList(String path){
		if(listcache.containsKey(path)){
			return listcache.get(path);
		}
		List<String> list = new ArrayList<String>();
		String prefix = MsSkyHomes.getPlugin().getConfig().getString("Messages.Prefix");
		prefix = ChatColor.translateAlternateColorCodes('&', prefix);
		if(MsSkyHomes.getPlugin().getConfig().isSet("Messages." + path)){
			for(String s : MsSkyHomes.getPlugin().getConfig().getStringList("Messages." + path)){
				list.add(ChatColor.translateAlternateColorCodes('&', s).replaceAll("(?i)<prefix>", prefix));
			}
		}
		listcache.put(path, list);
		return getStringList(path);
	}
	
	public void clearMessages(){
		cache.clear();
		listcache.clear();
	}
}

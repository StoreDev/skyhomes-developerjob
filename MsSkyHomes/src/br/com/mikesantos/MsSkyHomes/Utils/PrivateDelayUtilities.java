package br.com.mikesantos.MsSkyHomes.Utils;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;

public class PrivateDelayUtilities {

	
	protected HashMap<String, Long> cache = new HashMap<String, Long>();
	protected HashMap<String, Integer> timeDuration = new HashMap<String, Integer>();
	
	public void removeDelay(Player p){
		if(cache.containsKey(p.getName())){
			cache.remove(p.getName());
		}
		if(timeDuration.containsKey(p.getName())){
			timeDuration.remove(p.getName());
		}
	}
	
	public void setDelay(Player p, int tempo){
		long timestamp = System.currentTimeMillis() / 1000L;
		cache.put(p.getName(), timestamp);
		timeDuration.put(p.getName(), tempo);
	}
	
	public boolean noHasDelay(Player p) {
		if(!cache.containsKey(p.getName())){
			return true;
		}
		long _temp = System.currentTimeMillis() / 1000L;
		long currentTime = cache.get(p.getName());
		if(currentTime == 0L){
			return true;
		}
		if (_temp - currentTime > timeDuration.get(p.getName())) {
			removeDelay(p);
			return true;
		}
		return false;
	}

	private long invertTime(Player p) {
		long _temp = System.currentTimeMillis() / 1000L;
		long t = cache.get(p.getName());
		long i = _temp - t - timeDuration.get(p.getName());
		return ((i < 0) ? Math.abs(i) : i);
	}

	public String getDelay(Player p) {
		long seconds = invertTime(p);
		int dias = (int) TimeUnit.SECONDS.toDays(seconds);
		long horas = TimeUnit.SECONDS.toHours(seconds) - (dias * 24);
		long minutos = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
		long segundos = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
		String Final = "";
		if (dias != 0) {
			Final += Integer.toString(dias) + " d";
		}
		if (horas != 0) {
			if(!Final.equalsIgnoreCase("")) Final += " ";
			Final += Long.toString(horas) + " h";
		}
		if (minutos != 0) {
			if(!Final.equalsIgnoreCase("")) Final += " ";
			Final+= Long.toString(minutos) + " m";
		}
		if (segundos != 0) {
			if(!Final.equalsIgnoreCase("")) Final += " ";
			Final += Long.toString(segundos) + " s";
		}
		if (Final.equalsIgnoreCase(""))
			Final = "1 s";
		return Final;
	}
}

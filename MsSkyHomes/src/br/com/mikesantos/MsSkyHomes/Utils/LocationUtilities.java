package br.com.mikesantos.MsSkyHomes.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LocationUtilities {

	public static String serializeLocation(Location location){
		Location loc = location.clone();
		String ser = "";
		ser += "@world:" + loc.getWorld().getName() + ";";
		ser += "@x:" + loc.getX() + ";";
		ser += "@y:" + loc.getY() + ";";
		ser += "@z:" + loc.getZ() + ";";
		ser += "@yaw:" + loc.getYaw() + ";";
		ser += "@pitch:" + loc.getPitch();
		return ser;
	}
	
	public static Location deserializeLocation(String serialized){
		String[] s = serialized.split(";");
		World world = null;
		double x = 0;
		double y = 0;
		double z = 0;
		float yaw = 0;
		float pitch = 0;
		for(String l : s){
			String text = l.split(":")[0];
			String value = l.split(":")[1];
			if(text.equalsIgnoreCase("@world")){
				world = Bukkit.getWorld(value);
			}else if(text.equalsIgnoreCase("@x")){
				x = Double.valueOf(value);
			}else if(text.equalsIgnoreCase("@y")){
				y = Double.valueOf(value);
			}else if(text.equalsIgnoreCase("@z")){
				z = Double.valueOf(value);
			}else if(text.equalsIgnoreCase("@yaw")){
				yaw = Float.valueOf(value);
			}else if(text.equalsIgnoreCase("@pitch")){
				pitch = Float.valueOf(value);
			}
		}
		return new Location(world, x, y, z, yaw, pitch);
	}
}

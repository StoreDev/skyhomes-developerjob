package br.com.mikesantos.MsSkyHomes.Utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

	public static List<String> replace(String replace, String to, List<String> list){
		List<String> temp = new ArrayList<String>();
		list.forEach((s)->{
			temp.add(s.replaceAll(replace, to));
		});
		return temp;
	}
}

package br.com.mikesantos.MsSkyHomes;

import java.util.ArrayList;
import java.util.List;

import br.com.mikesantos.MsSkyHomes.Managers.VoteType;

public class Shop {

	protected String owner;
	protected String name;
	protected List<VoteType> votes = new ArrayList<VoteType>();
	public Shop(String owner, String name) {
		super();
		this.owner = owner;
		this.name = name;
	}
	
	public void addVote(VoteType type){
		votes.add(type);
	}
	
	public void clearVotes(){
		votes.clear();
	}
	
	public int getVotes(VoteType type){
		int i = 0;
		for(VoteType v : votes){
			if(v.equals(type)){
				i++;
			}
		}
		return i;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getOwnerName(){
		return this.owner;
	}
	
}

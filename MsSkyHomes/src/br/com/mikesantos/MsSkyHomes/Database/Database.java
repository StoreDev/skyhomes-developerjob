package br.com.mikesantos.MsSkyHomes.Database;

import java.io.File;
import java.sql.*;

public class Database {
	
    private final String connectionUri;
    private final String username;
    private final String password;
    private Connection connection;

    public Database(String hostname, String database, String username, String password){
        int port = 3306;
        connectionUri = String.format("jdbc:mysql://%s:%d/%s", hostname, port, database);
        this.username = username;
        this.password = password;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect();
        } catch (Exception sqlException) {
            close();
            sqlException.printStackTrace();
        }
    }
    
    public Database(File file){
    	try {
    		Class.forName("org.sqlite.JDBC");
    		this.connection = DriverManager.getConnection("jdbc:sqlite:" + file);
		} catch (Exception e) {}
    	this.username = null;
        this.password = null;
        this.connectionUri = null;
    }

    private void connect() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(connectionUri, username, password);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void close() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }

        } catch (SQLException ignored) {}
        connection = null;
    }

    public boolean isOpen() {
        try {
            connect();
            if(getConnection().isClosed()){
            	return false;
            }
        } catch (SQLException sqlException) {
            close();
            sqlException.printStackTrace();
            return false;
        }
        return true;
    }
    
}

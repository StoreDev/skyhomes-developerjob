package br.com.mikesantos.MsSkyHomes;

import java.util.concurrent.ConcurrentHashMap;

public class Shops {

	private String owner;
	private ConcurrentHashMap<String, Shop> shops = new ConcurrentHashMap<String, Shop>();
	public Shops(String owner) {
		super();
		this.owner = owner;
	}
	
	public void addShop(Shop shop){
		shops.put(shop.getName(), shop);
	}
	
	public Shop getShop(String shop){
		for(Shop v : shops.values()){
			if(v.getName().equalsIgnoreCase(shop)){
				return v;
			}
		}
		Shop sp = new Shop(this.owner, shop);
		addShop(sp);
		return sp;
	}
	
	public boolean hasShop(String shop){
		for(Shop v : shops.values()){
			if(v.getName().equalsIgnoreCase(shop)){
				return true;
			}
		}
		return false;
	}
	
	public String getOwner(){
		return this.owner;
	}
}

package br.com.mikesantos.MsSkyHomes.API;

import org.bukkit.entity.Player;

import br.com.mikesantos.MsSkyHomes.MsSkyHomes;
import br.com.mikesantos.MsSkyHomes.User;

public class API {

	protected MsSkyHomes plugin;

	public API(MsSkyHomes plugin) {
		super();
		this.plugin = plugin;
	}
	
	public User getUserByPlayer(Player p){
		return this.plugin.getDatabaseManager().getUserByPlayer(p);
	}
	
	public User getUserByUserName(String username){
		return this.plugin.getDatabaseManager().getUserByUsername(username);
	}
	
	public void showAllHomesOfPlayer(Player p){
		this.plugin.getInventoryManager().openAllHomes(p);
	}
	
	public void showAllShopsOfPlayerToPlayer(Player owner, Player see){
		User user = getUserByPlayer(owner);
		this.plugin.getInventoryManager().openShopOfPlayerToPlayer(user, see);
	}
}

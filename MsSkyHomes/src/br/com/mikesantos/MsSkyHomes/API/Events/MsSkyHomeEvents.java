package br.com.mikesantos.MsSkyHomes.API.Events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import br.com.mikesantos.MsSkyHomes.User;

public abstract class MsSkyHomeEvents extends Event implements Cancellable {

	protected Player player;
	protected String home;
	protected Location location;
	protected User user;

	public MsSkyHomeEvents(Player player, String home, Location location, User user) {
		super();
		this.player = player;
		this.home = home;
		this.location = location;
		this.user = user;
	}

	public Player getPlayer() {
		return this.player;
	}

	public String getHomeName() {
		return this.home;
	}

	public Location getLocation() {
		return this.location;
	}

	public User getUser() {
		return this.user;
	}

	protected boolean cancelled = false;

	@Override
	public boolean isCancelled() {
		return this.cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		this.cancelled = arg0;
	}

	private static final HandlerList handlers = new HandlerList();
	public HandlerList getHandlers() {
		return handlers;
	}

	public HandlerList getHandlerList() {
		return handlers;
	}

}

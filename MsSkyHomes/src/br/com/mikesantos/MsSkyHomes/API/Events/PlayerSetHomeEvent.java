package br.com.mikesantos.MsSkyHomes.API.Events;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import br.com.mikesantos.MsSkyHomes.User;

public class PlayerSetHomeEvent extends MsSkyHomeEvents{

	public PlayerSetHomeEvent(Player player, String home, Location location, User user) {
		super(player, home, location, user);
	}
}
